//
//  ViewController.swift
//  Gallery
//
//  Created by Oleg Ivaniv on 11/14/18.
//  Copyright © 2018 Gallery. All rights reserved.
//

import UIKit

class GalleryViewController: UICollectionViewController {
    
    private var activityIndicator: UIActivityIndicatorView!
    
    private let viewModel = GalleryViewModel(apiService: APIService())
    
    private var imageURLsList = [String]()
    private var images = [Int : UIImage]()
    
    fileprivate let spacing: CGFloat = 15
    fileprivate let inset: CGFloat = 15
    fileprivate var collectionViewWidth: CGFloat = 0.0
    fileprivate let cellHeightToWidthRatio: CGFloat = 0.75
    
    private var selectedItem = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupCollectionView()
        fetchImageList()
        
        title = "Gallery"
    }
    
    private func setupCollectionView() {
        collectionViewWidth = collectionView.bounds.width
        
        let flowLayout = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
        flowLayout.minimumInteritemSpacing = spacing
        flowLayout.sectionInset = UIEdgeInsets(top: inset, left: inset, bottom: inset, right: inset)
        
        activityIndicator = UIActivityIndicatorView(style: .gray)
        
        activityIndicator.startAnimating()
        activityIndicator.hidesWhenStopped = true
        collectionView.backgroundView = activityIndicator
    }
    
    private func fetchImageList() {
        self.viewModel.fetchImages(completionHandler: { [weak self] (imageList, error) in
            guard let self = self else { return }
            
            DispatchQueue.main.async {
                self.activityIndicator.stopAnimating()
                
                if error == nil, let imageList = imageList {
                    self.imageURLsList = imageList
                    self.collectionView.reloadData()

                    return
                }
                
                self.showEmptyCollectionViewBackground()
            }
        })
    }
    
    private func showEmptyCollectionViewBackground() {
        let label = UILabel()
        label.text = "It's a bit empty here... 🕸"
        label.textAlignment = .center
        label.textColor = UIColor.lightGray
        
        collectionView.backgroundView = label
    }
    
    // MARK: - UICollectionViewDataSource
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imageURLsList.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: ImageCollectionViewCell.self), for: indexPath)
            as! ImageCollectionViewCell
        
        cell.activityIndicator.startAnimating()
        
        if let image = images[indexPath.row] {
            cell.image = image
        } else {
            downloadImage(at: indexPath.row, for: cell)
        }
        
        return cell
    }
    
    private func downloadImage(at row: Int, for cell: ImageCollectionViewCell) {
        DispatchQueue.global(qos: .background).async {
            self.viewModel.downloadImage(at: self.imageURLsList[row], completionHandler: {[weak self] (image) in
                guard let self = self else { return}
                
                DispatchQueue.main.async {
                    cell.image = image
                    self.images[row] = image
                }
            })
        }
    }
    
    // MARK: - UICollectionViewDelegate
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if images.count != imageURLsList.count {
            return
        }
        
        selectedItem = indexPath.row
        performSegue(withIdentifier: "galleryToSlider", sender: nil)
    }
    
    // MARK: Seque
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destinationViewController = segue.destination as? SliderViewController {
            destinationViewController.position = selectedItem
            destinationViewController.images = images
        }
    }
}

// MARK: - UICollectionViewDelegateFlowLayout
extension GalleryViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout
        let spacing = flowLayout.sectionInset.left + flowLayout.sectionInset.right + flowLayout.minimumInteritemSpacing
        let size = (collectionViewWidth - spacing) / 2
        
        return CGSize(width: size, height: size * cellHeightToWidthRatio)
    }
}


