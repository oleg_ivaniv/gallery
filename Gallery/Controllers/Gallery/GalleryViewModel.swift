//
//  GalleryViewModel.swift
//  Gallery
//
//  Created by Oleg Ivaniv on 11/14/18.
//  Copyright © 2018 Gallery. All rights reserved.
//

import Foundation
import UIKit

class GalleryViewModel {
    var apiService: APIService
    
    required init(apiService: APIService) {
        self.apiService = apiService
    }
    
    func fetchImages(completionHandler: @escaping (_ imagesURLs: [String]?, _ error: Error?) -> ()) {
        DispatchQueue.global(qos: .background).async {
            return self.apiService.fetchImages(completionHandler: completionHandler)
        }
    }
    
    func downloadImage(at url: String, completionHandler: @escaping (_ image: UIImage?) -> ()) {
        guard let url = URL(string: url) else {
            completionHandler(nil)
            
            return
        }
        
        apiService.downloadImage(at: url, completionHandler: completionHandler)
    }
}
