//
//  ImageCollectionViewCell.swift
//  Gallery
//
//  Created by Oleg Ivaniv on 11/14/18.
//  Copyright © 2018 Gallery. All rights reserved.
//

import Foundation
import UIKit

class ImageCollectionViewCell: UICollectionViewCell {
    
    var image: UIImage? {
        didSet {
            activityIndicator.stopAnimating()
            
            if image != nil {
                imageView.image = image
                messageLabel.isHidden = true
            } else {
                messageLabel.isHidden = false
            }
        }
    }
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var messageLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        layer.borderWidth = 0.5
        layer.borderColor = UIColor.lightGray.cgColor
        messageLabel.isHidden = true
        
        activityIndicator.startAnimating()
    }
    
}
