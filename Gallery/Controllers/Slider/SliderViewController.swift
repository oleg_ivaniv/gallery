//
//  SliderViewController.swift
//  Gallery
//
//  Created by Oleg Ivaniv on 11/14/18.
//  Copyright © 2018 Gallery. All rights reserved.
//

import Foundation
import UIKit

class SliderViewController: UIViewController {
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var sliderPageControl: UIPageControl!
    
    private let imageHeightToWidthRatio: CGFloat = 0.75
    
    var images: [Int: UIImage]!
    var position = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupScrollView()
        setupPageControl()
        
        updateViewTitle()
    }
    
    private func setupScrollView() {
        let width = view.frame.width
        let height = view.frame.width * imageHeightToWidthRatio
        
        scrollView.frame = CGRect(x: 0, y: 0, width: width,
                                  height: view.frame.height)
        scrollView.contentSize = CGSize(width: view.frame.width * CGFloat(images.count), height: height)
        scrollView.isPagingEnabled = true
        
        let navigationBarHeight = UIApplication.shared.statusBarFrame.size.height +
            (self.navigationController?.navigationBar.frame.height ?? 0.0)
        
        for i in 0 ..< images.count {
            let imageView = UIImageView(image: images[i])
            
            imageView.frame = CGRect(x: width * CGFloat(i),
                                     y: view.frame.height / 2 - height / 2 - navigationBarHeight,
                                     width: width,
                                     height: height)
            scrollView.addSubview(imageView)
        }
        
        scrollView.contentOffset = CGPoint(x: CGFloat(position) * view.frame.width, y: 0)
    }
    
    private func setupPageControl() {
        sliderPageControl.numberOfPages = images.count
        sliderPageControl.currentPage = position
        view.bringSubviewToFront(sliderPageControl)
    }
    
    private func updateViewTitle() {
        title = "\(position + 1) of \(images.count)"
    }
}

extension SliderViewController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let pageIndex = Int(round(scrollView.contentOffset.x / view.frame.width))
        
        if pageIndex == position {
            return
        }
        
        position = pageIndex
        sliderPageControl.currentPage = position
        
        updateViewTitle()
    }
}
