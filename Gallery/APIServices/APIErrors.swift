//
//  APIErrors.swift
//  Gallery
//
//  Created by Oleg Ivaniv on 11/14/18.
//  Copyright © 2018 Gallery. All rights reserved.
//

import Foundation

enum APIErrorCode: Int {
    case unknown = -1
    case incorrectImagesURL = 1
    case missedResponseData = 2
    case incorrectResponseData = 3
    
    func description() -> String {
        switch self {
        case .incorrectImagesURL:
            return "Incorrect images URL"
            
        case .missedResponseData:
            return "Missed data in response"
            
        case .incorrectResponseData:
            return "Incorrect data in response"
            
        default:
            return "Unknown errors"
        }
    }
}

final class APIErrors {
    func error(for code: APIErrorCode) -> Error {
        return NSError(domain: "APIError",
                       code: code.rawValue,
                       userInfo: [NSLocalizedDescriptionKey: code.description()])
    }
}
