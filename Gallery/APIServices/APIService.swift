//
//  APIClient.swift
//  Gallery
//
//  Created by Oleg Ivaniv on 11/14/18.
//  Copyright © 2018 Gallery. All rights reserved.
//

import Foundation
import UIKit

final class APIService {
    
    func fetchImages(completionHandler: @escaping (_ imagesURLs: [String]?, _ error: Error?) -> ()) {
        guard let request = formImagesRequest() else {
            let error = APIErrors().error(for: .incorrectImagesURL)
            completionHandler(nil, error)
            
            return
        }
        
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            
            guard let data = data else {
                let missedResponseDataError = APIErrors().error(for: .missedResponseData)
                completionHandler(nil, missedResponseDataError)
                return
            }
            
            do {
                let json = try JSONSerialization.jsonObject(with: data, options:[])
                if let items = json as? [String] {
                    completionHandler(items, nil)
                    
                    return
                }
                
                let incorrectResponseDataError = APIErrors().error(for: .incorrectResponseData)
                completionHandler(nil, incorrectResponseDataError)
            }
            catch let error as NSError {
                completionHandler(nil, error)
            }
        }
        
        task.resume()
    }
    
    func downloadImage(at url: URL, completionHandler: @escaping (_ image: UIImage?) -> ()) {
        let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
            guard let data = data else {
                completionHandler(nil)
                
                return
            }
            
            if let image = UIImage(data: data) {
                completionHandler(image)
                
                return
            }
            
            completionHandler(nil)
        }
        
        task.resume()
    }
    
    private func formImagesRequest() -> URLRequest? {
        guard let url = URL(string: Endpoints.images) else {
            return nil
        }
        
        var request = URLRequest(url: url)
        request.setValue(Request.HeaderValue.applicationJson,
                         forHTTPHeaderField: Request.HeaderFields.accept)
        request.httpMethod = Request.Method.get
        
        return request
    }
}
