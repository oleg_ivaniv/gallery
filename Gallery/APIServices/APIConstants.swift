//
//  Constants.swift
//  Gallery
//
//  Created by Oleg Ivaniv on 11/14/18.
//  Copyright © 2018 Gallery. All rights reserved.
//

import Foundation

struct Endpoints {
    static let images = "http://petminder.tangosquared.com/api/get-media-urls"
}

struct Request {
    struct Method {
        static let post = "POST"
        static let get = "GET"
        static let put = "PUT"
        static let delete = "DELETE"
    }
    
    struct HeaderFields {
        static let accept = "Accept"
    }
    
    struct HeaderValue {
        static let applicationJson = "application/json"
    }
}
